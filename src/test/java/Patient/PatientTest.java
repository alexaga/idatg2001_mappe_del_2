package Patient;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientTest {

    private Patient patient;

    @BeforeEach
    void createTestPatients() {
        patient = new Patient("Hans","Hansen",
                "12345678900","Petter Petterson","Hjerteinnfarkt");
    }

    @Test
    @DisplayName("Setting first name")
    void setFirstNamePositive() {
        patient.setFirstName("Felix");
        assertEquals("Felix",patient.getFirstName());
    }

    @Test
    @DisplayName("Setting first name as null")
    void setFirstNameNull() {
        String exceptionMessage = "";
        try {
            patient.setFirstName(null);
        } catch (IllegalArgumentException iae) {
            exceptionMessage = iae.getMessage();
        }
        assertTrue(exceptionMessage.contains("First name can not be empty"));
    }

    @Test
    @DisplayName("Setting first name as empty string")
    void setFirstNameEmptyString() {
        String exceptionMessage = "";
        try {
            patient.setFirstName("");
        } catch (NullPointerException npe) {
            exceptionMessage = npe.getMessage();
            System.out.println(exceptionMessage);
        }
        assertTrue(exceptionMessage.contains("First name can not be blank"));
    }


    @Test
    @DisplayName("Setting social security number")
    void setSocialSecurityNumber() {
        patient.setSocialSecurityNumber("12312312312");
        assertEquals("12312312312",patient.getSocialSecurityNumber());
    }

    @Test
    @DisplayName("Setting social security number as null")
    void setSocialSecurityNumberNull() {
        String exceptionMessage = "";
        try {
            patient.setSocialSecurityNumber(null);
        } catch (IllegalArgumentException iae) {
            exceptionMessage = iae.getMessage();
        }
        assertTrue(exceptionMessage.contains("Social security number can not be empty"));
    }

    @Test
    @DisplayName("Setting social security number to a number not with 11 digits")
    void setSocialSecurityNumberNegative() {
        String exceptionMessage = "";
        try {
            patient.setSocialSecurityNumber("123");
        } catch (IllegalArgumentException iae) {
            exceptionMessage = iae.getMessage();
        }
        assertTrue(exceptionMessage.contains("Social security number must be 11 characters long"));
    }

}