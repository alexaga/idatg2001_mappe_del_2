package Patient;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import Exception.RemoveException;

import static org.junit.jupiter.api.Assertions.*;

class PatientRegisterTest {

    PatientRegister patientRegister;
    Patient patient1;
    Patient patient2;
    Patient patientNotInList;

    @BeforeEach
    void createTestRegister() {
        patientRegister = new PatientRegister();
        patient1 = new Patient("Hans", "Hansen",
                "12345678900", "Petter Petterson", "Hjerteinnfarkt");
        patient2 = new Patient("Hanne", "Berglund",
                "12345678901", "Patrik kalsrud", "Kreft");
        patientNotInList = new Patient("Pia","Pan","10987654321","Fabionachi","Polio");

        patientRegister.addPatient(patient1);
        patientRegister.addPatient(patient2);
    }

    @Test
    @DisplayName("Editing patient - Positive")
    void editPatient() {
        patientRegister.editPatient(patient1,"Pål","Pan","10987654321","Fabio","Frisk");
        assertEquals("Patient name:: Pål Pan, SSN: 10987654321. General Practitioner:: Fabio, diagnosis:: Frisk",patient1.toString());
    }

    @Test
    @DisplayName("Editing patient - Negative, null values")
    void editPatientNull() {
        assertThrows(IllegalArgumentException.class,() -> {patientRegister.editPatient(patient1, null, null, null, null, null);});
    }

    @Test
    @DisplayName("Editing patient - Negative, empty values")
    void editPatientEmpty() {
        assertThrows(NullPointerException.class,() -> {patientRegister.editPatient(patient1, "", "", "", "", "");});
    }

    @Test
    @DisplayName("Remove patient - Positive")
    void removePatient() {
        int patientRegisterSize = patientRegister.getPatientList().size()-1;
        try {
            patientRegister.remove(patient1);
        } catch (RemoveException re) {
            re.getMessage();
        }
        assertEquals(patientRegisterSize,patientRegister.getPatientList().size());
    }

    @Test
    @DisplayName("Remove patient - Negative, null values")
    void removePatientNotInList() {
        Patient patient = new Patient("Pia","Pan","10987654321","Fabionachi","Polio");

        String exceptionMessage = "";
        try {
            patientRegister.remove(patient);
        } catch (RemoveException re) {
            exceptionMessage = re.getMessage();
        }
        assertTrue(exceptionMessage.contains("Patient is not in patient list"));
    }

    @Test
    @DisplayName("Remove patient - Negative, empty values")
    void removeNullPatient() {
        Patient patient = null;

        String exceptionMessage = "";
        try {
            patientRegister.remove(patient);
        } catch (RemoveException re) {
            exceptionMessage = re.getMessage();
        }
        assertTrue(exceptionMessage.contains("Patient can not be blank"));
    }

    @Test
    @DisplayName("Remove patient - Negative, empty list")
    void removeFromEmptyList() {
        PatientRegister emptyRegister = new PatientRegister();

        String exceptionMessage = "";
        try {
            emptyRegister.remove(patientNotInList);
        } catch (RemoveException re) {
            exceptionMessage = re.getMessage();
        }
        assertTrue(exceptionMessage.contains("Patient list is empty"));
    }
}