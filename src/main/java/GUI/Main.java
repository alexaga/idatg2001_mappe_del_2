package GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.FileInputStream;

public class Main extends Application {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader();

        String fxmlPath = "src/main/resources/FXML/Main.fxml";

        FileInputStream inputStream = new FileInputStream(fxmlPath);

        AnchorPane root = fxmlLoader.load(inputStream);

        Scene scene = new Scene(root);

        stage.setScene(scene);

        stage.setTitle("Patient Register");

        stage.setResizable(false);

        stage.show();
    }
}
