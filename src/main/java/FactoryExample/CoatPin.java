package FactoryExample;

/**
 * The type Coat pin.
 */
public class CoatPin extends CoatDesigner {

    /**
     * Instantiates a new Coat pin for coat.
     *
     * @param doctorCoat the doctor coat
     */
    public CoatPin(DoctorCoat doctorCoat) {
        super(doctorCoat);
    }

    /**
     * Instantiates a new Coat pin for coat.
     */
    public CoatPin() {
        super();
    }

    @Override
    public void haveCoat() {
        super.haveCoat();
        System.out.println("Adding Pin To Coat");
    }
}
