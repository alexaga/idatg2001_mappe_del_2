package FactoryExample;

/**
 * The type Coat designer.
 */
public class CoatDesigner implements DoctorCoat {

    /**
     * The Doctor coat.
     */
    protected DoctorCoat doctorCoat;

    /**
     * Instantiates a new Coat designer.
     *
     * @param doctorCoat the doctor coat
     */
    public CoatDesigner(DoctorCoat doctorCoat) {
        this.doctorCoat=doctorCoat;
    }

    /**
     * Instantiates a new Coat designer.
     */
    public CoatDesigner() {

    }

    @Override
    public void haveCoat() {
        this.doctorCoat.haveCoat();
    }
}
