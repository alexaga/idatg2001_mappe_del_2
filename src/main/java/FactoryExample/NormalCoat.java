package FactoryExample;

/**
 * The type Normal coat.
 */
public class NormalCoat implements DoctorCoat {

    @Override
    public void haveCoat() {
        System.out.println("Have normal coat");
    }
}
