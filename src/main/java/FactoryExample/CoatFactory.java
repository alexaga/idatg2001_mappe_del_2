package FactoryExample;

/**
 * This is a factory example not part of the PatientRegister project
 *
 * The type Coat factory.
 */
public class CoatFactory {


    /**
     * Gets doctor coat.
     *
     * @param coatDecor the coat decor
     * @return the doctor coat
     */
    public static DoctorCoat getDoctorCoat(String coatDecor) {
        DoctorCoat doctorCoat = null;

        switch(coatDecor)
        {
            case "PIN":
                doctorCoat = new CoatPin();
                break;
            case "RED":
                doctorCoat = new ColorRed();
                break;
            default:
                doctorCoat = null;
        }
        return doctorCoat;
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        DoctorCoat redCoatWithPin = new ColorRed(new CoatPin(new NormalCoat()));
        redCoatWithPin.haveCoat();

        DoctorCoat plainCoat = new NormalCoat();
        plainCoat.haveCoat();
    }
}
