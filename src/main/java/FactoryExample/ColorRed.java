package FactoryExample;

/**
 * The type Color red.
 */
public class ColorRed extends CoatDesigner {

    /**
     * Instantiates a new Color red for coat.
     *
     * @param doctorCoat the doctor coat
     */
    public ColorRed(DoctorCoat doctorCoat) {
        super(doctorCoat);
    }

    /**
     * Instantiates a new Color red for coat.
     */
    public ColorRed() {

    }

    @Override
    public void haveCoat() {
        super.haveCoat();
        System.out.println("Color coat red");
    }
}
