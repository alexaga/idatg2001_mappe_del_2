package FactoryExample;

/**
 * The interface Doctor coat.
 */
public interface DoctorCoat {
    /**
     * Have coat method.
     */
    public void haveCoat();
}
