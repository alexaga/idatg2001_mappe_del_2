package Patient;

import Exception.RemoveException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Patient register.
 */
public class PatientRegister {
private ArrayList<Patient> patientList;

    /**
     * Instantiates a new Patient register.
     */
    public PatientRegister() {
        this.patientList = new ArrayList<>();
    }

    /**
     * Add patient.
     *
     * @param patient the patient
     */
    public void addPatient(Patient patient) {
        this.patientList.add(patient);
    }

    /**
     * Gets patient list.
     *
     * @return the patient list
     */
    public List<Patient> getPatientList()
    {
        return this.patientList;
    }

    /**
     * Edit patient.
     *
     * @param patient              the patient
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     * @param generalPractitioner  the general practitioner
     * @param diagnosis            the diagnosis
     * @return the patient
     */
    public Patient editPatient(Patient patient, String firstName, String lastName, String socialSecurityNumber,
                               String generalPractitioner, String diagnosis) {
        patient.setFirstName(firstName);
        patient.setLastName(lastName);
        patient.setSocialSecurityNumber(socialSecurityNumber);
        patient.setGeneralPractitioner(generalPractitioner);
        patient.setDiagnosis(diagnosis);
        return patient;
    }

    /**
     * Remove patient from patientregister
     *
     * @param patient the patient
     * @throws RemoveException the remove exception
     */
// Reused some of my code from Hospital project
    public void remove(Patient patient) throws RemoveException {
        if (patient == null) {
            throw new RemoveException("Patient can not be blank");
        } else if (patientList.isEmpty()) {
            throw new RemoveException("Patient list is empty");
        } else if (patientList.contains(patient)) {
            this.patientList.remove(patient);
        } else {
            throw new RemoveException("Patient is not in patient list");
        }
    }
}
