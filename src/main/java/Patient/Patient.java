package Patient;

//Used much of the patient and person code from mappe 1

/**
 * The type Patient.
 */
public class Patient {
    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    public Patient(String firstName, String lastName, String socialSecurityNumber,
                   String generalPractitioner, String diagnosis) {
        setFirstName(firstName);
        setLastName(lastName);
        setSocialSecurityNumber(socialSecurityNumber);
        setGeneralPractitioner(generalPractitioner);
        setDiagnosis(diagnosis);
    }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        if(firstName == null) {
            throw new IllegalArgumentException("First name can not be empty");
        } else if (firstName.isBlank()) {
            throw new NullPointerException("First name can not be blank");
        }
        this.firstName = firstName;
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name.
     *
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        if(lastName == null) {
            throw new IllegalArgumentException("Last name can not be empty");
        } else if (lastName.isBlank()) {
            throw new NullPointerException("Last name can not be blank");
        }
        this.lastName = lastName;
    }

    /**
     * Gets social security number.
     *
     * @return the social security number
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Sets social security number.
     *
     * @param socialSecurityNumber the social security number
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        if (socialSecurityNumber == null) {
            throw new IllegalArgumentException("Social security number can not be empty");
        }
        else if (!(socialSecurityNumber.length() == 11)) {
            throw new IllegalArgumentException("Social security number must be 11 characters long");
        }
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Gets generalPractitioner
     *
     * @return the generalPractitioner
     */
    public String getGeneralPractitioner() { return generalPractitioner;}

    /**
     * Sets generalpractitioner.
     *
     * @param generalPractitioner the generalpractitioner
     */
    public void setGeneralPractitioner(String generalPractitioner) {
        if(generalPractitioner == null) {
            throw new IllegalArgumentException("Invalid general practitioner");
        }
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Gets diagnosis
     *
     * @return the diagnosis
     */
    public String getDiagnosis() {return diagnosis;}

    /**
     * Sets diagnosis.
     *
     * @param diagnosis the patients diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        if(diagnosis == null) {
            throw new IllegalArgumentException("Invalid diagnosis");
        }
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return "Patient name:: "+firstName+" "+lastName+", SSN: "+socialSecurityNumber
                +". General Practitioner:: "+generalPractitioner+", diagnosis:: "+diagnosis;
    }
}
