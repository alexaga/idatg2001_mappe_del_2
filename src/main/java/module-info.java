module PatientRegister {
    requires  javafx.graphics;
    requires  javafx.controls;
    requires  javafx.fxml;
    requires  log4j;
    exports GUI;
    exports Controllers;
    opens Patient to javafx.base;
}