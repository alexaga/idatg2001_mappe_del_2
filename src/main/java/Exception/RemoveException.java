package Exception;

import java.io.Serializable;

// Retrieved from Hospital project

/**
 * The type Remove exception.
 */
public class RemoveException extends Exception implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new Remove exception.
     *
     * @param message the message
     */
    public RemoveException(String message) {
        super(message);
    }
}
