package Utility;

import Controllers.ControllerMain;
import Patient.Patient;
import Patient.PatientRegister;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.*;

/**
 * The type Export import class.
 */
public class ExportImportClass {

    /**
     * The Create alert.
     */
    AlertClass createAlert;
    /**
     * The Status.
     */
    String status;

    private static final Logger LOGGER = Logger.getLogger(ControllerMain.class.getName());

    /**
     * Import csv file.
     *
     * @param stage           the stage
     * @param patientRegister the patient register
     * @return the status of how the operation went
     */
    public String importCSV(Stage stage, PatientRegister patientRegister) {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose CSV file");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("csv", "*.csv"));
            File file = fileChooser.showOpenDialog(stage);

            BufferedReader csvReader = new BufferedReader(new FileReader(file));
            String row;
            csvReader.readLine();
            int exceptionCounter = 0;
            while ((row = csvReader.readLine()) != null) {
                try {
                    String[] data = row.split(";");
                    if (data.length == 5) {
                        patientRegister.addPatient(new Patient(data[0], data[1],
                                data[3], data[2], data[4]));
                    } else if (data.length == 4) {
                        patientRegister.addPatient(new Patient(data[0], data[1],
                                data[3], data[2], ""));
                    }
                }catch (IllegalArgumentException iae) {
                        exceptionCounter++;
                    }
            }
            csvReader.close();
            if(exceptionCounter != 0) {
                createAlert = new AlertClass();
                createAlert.createInformationAlert("Wrong inputs",exceptionCounter+" patients with illegal arguments\nnot added to register.");
                LOGGER.warn(exceptionCounter+" patients not added because of illegal arguments");
                status = exceptionCounter+" patients was not imported";
            } else {
                status = "Import successfull";
            }

        } catch (FileNotFoundException fnfe) {
            status = "Import failed";
            LOGGER.warn("File was not found. "+fnfe.getMessage());
        } catch (IOException ioe) {
            ioe.getMessage();
            status = "Import failed";
        } catch (NullPointerException npe) {
            LOGGER.fatal("Import gave a null value. "+npe.getMessage());
            //createAlert = new AlertClass();
            //createAlert.createInformationAlert("Wrong File Type Selected","You need to select a .csv file");
            status = "Import failed";
        }
        return status;
    }

    /**
     * Export csv file.
     *
     * @param stage           the stage
     * @param patientRegister the patient register
     * @return the status of how the operation went
     */
    public String exportCSV(Stage stage, PatientRegister patientRegister) {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save As");
            fileChooser.setInitialFileName("PatientRegister");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("csv", "*.csv"));
            File file = fileChooser.showSaveDialog(stage);

            BufferedWriter csvWriter = new BufferedWriter(new FileWriter(file));
            StringBuilder columns = new StringBuilder();
            columns.append("First Name;Last Name;General Practitioner;Social Security Number;Diagnosis");
            csvWriter.write(columns.toString());
            StringBuilder csvFile;

            for(Patient patient: patientRegister.getPatientList()) {
                csvFile = new StringBuilder();
                csvFile.append(patient.getFirstName()+";"+patient.getLastName()+
                        ";"+patient.getGeneralPractitioner()+";"+patient.getSocialSecurityNumber()+
                        ";"+patient.getDiagnosis());
                csvWriter.newLine();
                csvWriter.write(csvFile.toString());
            }

            csvWriter.flush();
            csvWriter.close();
            status = "Export successfull";
        } catch (IOException ioe) {
            LOGGER.warn(ioe.getMessage());
            status = "Export failed";
        } catch (NullPointerException npe) {
            LOGGER.warn("Export gave a null value. " + npe.getMessage());
            status = "Export failed";
        }
        return status;
    }
}
