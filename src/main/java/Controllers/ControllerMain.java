package Controllers;

import Patient.*;
import Patient.PatientRegister;
import Utility.AlertClass;
import Utility.ExportImportClass;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * The type Controller main.
 */
public class ControllerMain implements Initializable {

    private static PatientRegister patientRegister;
    private AlertClass createAlert;
    private ExportImportClass exportImport;

    private static final Logger LOGGER = Logger.getLogger(ControllerMain.class.getName());

    private static Patient patientToBeEdited;
    private static Patient patientToRemove;
    private static String editOrAddPatient;

    /**
     * The Add patient.
     */
    public Button addPatient;

    /**
     * The Status bar.
     */
    public Label statusBar;

    /**
     * The Patient table view.
     */
    public TableView<Patient> patientTableView;

    /**
     * ObservableList displayed in tableview
     */
    private ObservableList<Patient> patientListWrapper;

    /**
     * Gets controller patient register.
     *
     * @return the controller patient register
     */
    public static PatientRegister getControllerPatientRegister() {
        return patientRegister;
    }

    /**
     * Gets patient to be edited.
     *
     * @return the patient to be edited
     */
    public static Patient getPatientToBeEdited() {
        return patientToBeEdited;
    }

    /**
     * Gets edit or add patient.
     *
     * @return the edit or add patient
     */
    public static String getEditOrAddPatient() {
        return editOrAddPatient;
    }

    /**
     * Gets selected patient.
     *
     * @return the selected patient
     */
    public Patient getSelectedPatient() {
        return patientTableView.getSelectionModel().getSelectedItem();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        patientListWrapper = FXCollections.observableArrayList();
        patientRegister = new PatientRegister();
        setUpTable();
    }

    /**
     * Sets up tableview and selects list to display.
     */
    public void setUpTable() {
        // Creating all columns
        TableColumn<Patient, String> firstNameCol = new TableColumn<>("First name");
        firstNameCol.setCellValueFactory(new PropertyValueFactory<Patient, String>("firstName"));

        TableColumn<Patient, String> lastNameCol = new TableColumn<>("Last name");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<Patient, String>("lastName"));

        TableColumn<Patient, String> SSNCol = new TableColumn<>("Social security number");
        SSNCol.setCellValueFactory(new PropertyValueFactory<Patient, String>("socialSecurityNumber"));

        TableColumn<Patient, String> gPCol = new TableColumn<>("General Practitioner");
        gPCol.setCellValueFactory(new PropertyValueFactory<Patient, String>("generalPractitioner"));

        TableColumn<Patient, String> diagnosisCol = new TableColumn<>("Diagnosis");
        diagnosisCol.setCellValueFactory(new PropertyValueFactory<Patient, String>("diagnosis"));

        // All column widths set and set not reorderable and resizable
        firstNameCol.setReorderable(false);firstNameCol.setResizable(false);firstNameCol.setPrefWidth(119);
        lastNameCol.setReorderable(false);lastNameCol.setResizable(false);lastNameCol.setPrefWidth(119);
        SSNCol.setReorderable(false);SSNCol.setResizable(false);SSNCol.setPrefWidth(120);
        diagnosisCol.setReorderable(false);diagnosisCol.setResizable(false);diagnosisCol.setPrefWidth(120);
        gPCol.setReorderable(false);gPCol.setResizable(false);gPCol.setPrefWidth(120);

        patientTableView.setItems(patientListWrapper);
        patientTableView.getColumns().addAll(firstNameCol, lastNameCol, SSNCol, gPCol, diagnosisCol);
    }

    /**
     * Open add patient window.
     *
     * @param event the event
     */
    @FXML
    public void openAddPatient(ActionEvent event) {
        editOrAddPatient = "Add";
        openPatientWindow();
    }

    /**
     * Open patient window.
     */
    public void openPatientWindow() {
        Stage patientStage = new Stage();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/FXML/PatientWindow.fxml"));
            Parent rootAddPatient = (Parent) fxmlLoader.load();

            patientStage.initModality(Modality.APPLICATION_MODAL);
            if(editOrAddPatient.equalsIgnoreCase("Add")) {
                patientStage.setTitle("Add Patient");
            } else {
                patientStage.setTitle("Edit Patient");
            }
            patientStage.setResizable(false);
            patientStage.setScene(new Scene(rootAddPatient));
            patientStage.show();
        } catch(IOException ioe) {
            LOGGER.warn(ioe.getMessage());
        }
        patientStage.setOnHiding(evt -> {updateObservableList();
        setStatusBar(ControllerPatient.getStatus());});
    }

    /**
     * open edit patient window.
     */
    public void editPatient() {
        patientToBeEdited = getSelectedPatient();
        editOrAddPatient = "Edit";
        if(patientToBeEdited != null) {
            openPatientWindow();
        } else {
            createAlert = new AlertClass();
            createAlert.createInformationAlert("You have not selected a patient to edit",
                    "Select a patient to edit.");
        }
    }

    /**
     * Remove patient.
     */
    public void removePatient() {
        patientToRemove = getSelectedPatient();

        if(patientToRemove != null) {
            createAlert = new AlertClass();
            if(createAlert.createConfirmationAlert("Are you sure you want to\nRemove: "
                    +patientToRemove.getFirstName()+"?","Click OK to confirm")) {
                try {
                    patientRegister.getPatientList().remove(patientToRemove);
                    updateObservableList();
                    setStatusBar("Patient removed successfully");
                } catch (NullPointerException npe) {
                    LOGGER.warn("Got a null value. " + npe.getMessage());
                }
            }
        } else {
            createAlert = new AlertClass();
            createAlert.createInformationAlert("You have not selected a patient to remove",
                    "Select a patient to remove.");
        }
    }

    /**
     * Gets the observablelist displayed in tableview
     * @return patientListWrapper
     */
    //Code inspired by javafx 17 example project on blackboard
    private ObservableList<Patient> getPatientListWrapper() {
        patientListWrapper = FXCollections.observableArrayList(this.patientRegister.getPatientList());
        return patientListWrapper;
    }

    /**
     * Updates observable list displayed in tableview.
     */
    //Code inspired by javafx 17 example project on blackboard
    public void updateObservableList() {
        patientListWrapper.setAll(this.patientRegister.getPatientList());
    }

    /**
     * Exits application.
     */
    public void exit(){
        System.exit(0);
    }

    /**
     * Opens about info alert.
     */
    public void about() {
        createAlert = new AlertClass();
        createAlert.createInformationAlert("Patient Register\nVersion: 1.0",
                "Application Created By\nAlexander Gatland\n"+ LocalDate.now());
    }

    /**
     * Method to import csv file.
     */
    public void importCSV() {
        exportImport = new ExportImportClass();
        Stage stage = (Stage) addPatient.getScene().getWindow();
        String status = exportImport.importCSV(stage, patientRegister);
        updateObservableList();
        setStatusBar(status);
    }

    /**
     * Method to export csv file.
     */
    public void exportCSV() {
        exportImport = new ExportImportClass();
        Stage stage = (Stage) addPatient.getScene().getWindow();
        String status = exportImport.exportCSV(stage, patientRegister);
        setStatusBar(status);
    }

    /**
     * Sets status bar.
     *
     * @param status the status
     */
    public void setStatusBar(String status) {
        statusBar.setText(status);
    }
}