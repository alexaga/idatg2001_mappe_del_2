package Controllers;

import Patient.Patient;
import Utility.AlertClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * The type Controller patient.
 */
public class ControllerPatient implements Initializable {

    /**
     * The First name.
     */
    @FXML
    public TextField firstName;

    /**
     * The Last name.
     */
    @FXML
    public TextField lastName;

    /**
     * The SSN.
     */
    @FXML
    public TextField sSN;

    /**
     * The General practitioner.
     */
    @FXML
    public TextField generalPractitioner;

    /**
     * The Diagnosis.
     */
    @FXML
    public TextField diagnosis;

    /**
     * The Cancel button.
     */
    @FXML
    public Button cancelButton;

    /**
     * The Ok button.
     */
    @FXML
    public Button okButton;

    /**
     * The Page title.
     */
    @FXML
    public Label pageTitle;

    private static final Logger LOGGER = Logger.getLogger(ControllerPatient.class.getName());

    private Patient selectedPatient;

    private AlertClass alert;

    private static String status;

    /**
     * Gets status.
     *
     * @return the status
     */
    public static String getStatus() {
        return status;
    }

    /**
     * Method to either add or edit patient.
     *
     * @param event the event
     */
    @FXML
    public void addOrEdit(ActionEvent event) {
            if(firstName.getText().isBlank() || lastName.getText().isBlank() || sSN.getText().isBlank()) {
                alert = new AlertClass();
                alert.createInformationAlert("Wrong Input",
                        "First name, Last name and\nSocial security number must be filled in.");
            } else {
                if(!(sSN.getText().length() == 11)) {
                    alert = new AlertClass();
                    alert.createInformationAlert("Wrong SSN length",
                            "Social security number must\nbe 11 digits long.");
                } else {
                    if(ControllerMain.getEditOrAddPatient().equalsIgnoreCase("Add")) {
                        try {
                            ControllerMain.getControllerPatientRegister().addPatient(new Patient(
                                    firstName.getText(), lastName.getText(), sSN.getText()
                                    , generalPractitioner.getText(), diagnosis.getText()));
                        } catch (IllegalArgumentException iae) {
                            LOGGER.warn("Illegal argument. "+iae.getMessage());
                        } catch (NullPointerException npe) {
                            LOGGER.warn("Got null value. "+npe.getMessage());
                        }

                        status = "Patient add successfull";
                        closeWindow();
                    }else {
                        try {
                            ControllerMain.getControllerPatientRegister().editPatient(selectedPatient,
                                    firstName.getText(), lastName.getText(), sSN.getText(),
                                    generalPractitioner.getText(), diagnosis.getText());
                        } catch (IllegalArgumentException iae) {
                            LOGGER.warn("Illegal argument. "+iae.getMessage());
                        } catch (NullPointerException npe) {
                            LOGGER.warn("Got null value. "+npe.getMessage());
                        }

                        status = "Patient edit successfull";
                        closeWindow();
                    }
                }
            }
        }

    /**
     * Close window.
     */
    @FXML
    public void closeWindow(){
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

    /**
     * Fill text fields with selected patient to edit.
     */
    public void fillTextFields() {
        firstName.setText(selectedPatient.getFirstName());
        lastName.setText(selectedPatient.getLastName());
        sSN.setText(selectedPatient.getSocialSecurityNumber());
        generalPractitioner.setText(selectedPatient.getGeneralPractitioner());
        diagnosis.setText(selectedPatient.getDiagnosis());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if(ControllerMain.getEditOrAddPatient().equalsIgnoreCase("Edit")) {
            status = "Patient not edited";
            pageTitle.setText("Edit Patient");
            selectedPatient = ControllerMain.getPatientToBeEdited();
            fillTextFields();
        } else {
            status = "Patient not added";
        }
    }
}